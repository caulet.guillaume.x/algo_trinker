const { includes } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let x = [];
    for (let i of p) {
      if (i.gender === "Male") {
        x.push(i);
      }
    }
    return x;
  },

  allFemale: function (p) {
    let y = [];
    for (let i of p) {
      if (i.gender === "Female") {
        y.push(i);
      }
    }
    return y;
  },
  // ____________________________________LEVEL 1
  nbOfMale: function (p) {
    return this.allMale(p).length;
  },

  nbOfFemale: function (p) {
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function (p) {
    let mi = [];
    for (let i of p) {
      if (i.looking_for === "M") {
        mi.push(i);
      }
    }

    return mi.length;
  },

  nbOfFemaleInterest: function (p) {
    let fi = [];
    for (let i of p) {
      if (i.looking_for === "F") {
        fi.push(i);
      }
    }

    return fi.length;
  },

  nbOfHsalary: function (p) {
    ////créer une fonction pour calculer le nbre de salaire sup à 2000
    let n = []; //créer un tableau (n)
    for (let i of p) {
      //boucler (faire défiler les lignes i du tableau (p)=> people)
      x = i.income.substring(1); //définir (x) =  salaire (i.income) en retirant le dollar (substring)
      if (parseFloat(x) > 2000) {
        //si le nombre x (parseFloat(x)) est superieur à 2000
        n.push(i); //rajouter la ligne (i) dans le tableau (n)
      }
    }
    return n.length; //renvoyer la longueur du tableau (n) => nbre de salaire sup à 2000
  },

  nbOfLdrama: function (p) {
    //créer une fonction pour calculer le nbre de personne préférant les Drama
    let n = []; //créer un tableau (n)
    for (let i of p) {
      //boucler (faire défiler les lignes i du tableau p)
      if (i.pref_movie.includes("Drama")) {
        // si la ligne pref_movie contient Drama
        n.push(i); //rajoute la ligne dans le tableau (n)
      }
    }
    return n.length; //renvoi la longueur du tableau (n) => nbre de personne aimant les Drama
  },

  nbOfFemaleLsf: function (p) {
    //créer une fonction pour calculer le nbre de femmes aimant les Sci-Fi
    let n = []; //créer un tableau (n)
    for (let i of this.allFemale(p)) {
      //boucler (faire défiler les lignes dans le tableau(p) en ne prenant que les femmes (this.allFemale(p) ))
      if (i.pref_movie.includes("Sci-Fi")) {
        // si la ligne pref_movie contient Sci-Fi
        n.push(i); //rajouter la ligne dans le tableau (n)
      }
    }
    return n.length; //renvoyer la longueur du tableau (n) => nbre de femmes aimant les Sci-Fi
  },

  // ___________________________________LEVEL 2

  nb0LdocMon: function (p) {
    //créer une fonction pour calculer le nbre de personne aimant les Documentary et ayant un salaire sup à 1482
    let n = []; //créer un tableau (n)
    for (let i of p) {
      //boucler (faire défiler les lignes i du tableau (p)=> people)
      x = i.income.substring(1); //définir (x) =  salaire (i.income) en retirant le dollar (substring)
      if (i.pref_movie.includes("Documentary") && parseFloat(x) > 1482) {
        //si la ligne pref_movie contient Documentary et que le salaire est sup à 1482
        n.push(i); //rajouter la ligne dans le tableau (n)
      }
    }
    return n.length; //renvoyer la longueur du tableau (n) => nbre de personne aimant les Documentary et gagnant plus de 1482
  },

  L_nfnidSh: function (p) {
    //créer une fonction pour calculer le nbre de personne ayant un salaire sup à 4000
    let n = []; //créer un tableau (n)
    for (let i of p) //boucler (faire défiler les lignes i du tableau (p))
      if (parseFloat(i.income.substring(1)) > 4000) {
        //si la ligne i.income (salaire) est sup à 4000
        n.push(i.id, i.first_name, i.last_name, i.income); //rajoute la ligne avec les attributs prénom(first_name) nom (last_name) salaire (income) au tableau (n)
      }
    return n; //renvoyer le tableau (n) comprenant toutes les personnes ayant un salaire sup à 4000 ( prénom nom salaire)
  },

  MRm: function (p) {
    //créer une fonction pour calculer l'homme le plus riche ( plus haut salaire)
    let Th = this.allMale(p); //définir le tableau (th) comprenant seulement les hommes
    let M_Rm; //définir un objet M_Rm (homme le plus riche)
    let Hs = 0; //définir un objet Hs (haut salaire)
    for (let i of Th) {
      //boucler (faire défiler les lignes i du tableau(th)=> tout les hommes)
      if (this.Salary(i) > Hs) {
        //si le salaire de la ligne i est sup à Hs=> définit de base a 0
        M_Rm = i; //l'homme le plus riche = ligne i
        Hs = this.Salary(i); //haut salaire = salaire i
      }
    }
    return [M_Rm.last_name, M_Rm.id]; //revoyer un tableau avec last_name et id de M_Rm
  },
  AvSalary: function (p) {
    //créer une fonction pour calculer la moyenne de salaire
    let n = 0; //définir un objet egal a 0
    for (let i of p) {
      //boucler (faire défiler les lignes i du tableau(p)=> people)
      n += parseFloat(i.income.substring(1)); //ajouter les salaires des lignes i dans (n)
    }
    let result = n / p.length; //définir le résultat => n(somme de tous les salaires)/ la longueur du tableau (p)=> nbre de personne totale
    return result; //renvoyer le résultat
  },

  MeSalary: function (p) {
    //créer une fonction pour calculer la médiane des salaires
    let allS = []; //définir un tableau pour stocker les salaires
    for (let i of p) {
      //boucler (faire défiler les lignes i du tableau (p))
      result = i.income; //result => les salaires
      value = parseFloat(result.slice(1)); //value => addition des salaires
      allS.push(value);
    }
    allS = allS.sort((a, b) => a - b);
    return allS[allS.length / 2];
  },

  nb0Hn: function (p) {
    //créer une fonction pour calculer le nbre de personne vivant dans l'hémisphère nord
    let n = []; //créer un tableau (n)
    for (i of p) {
      //boucler (faire défiler les lignes i du tableau (p))
      if (i.latitude > 0) {
        //si la valeur de la ligne i(latitude) est sup à 0(=>équateur)
        n.push(i); //rajouter la ligne dans le tableau (n)
      }
    }
    return n.length; //renvoyer la longueur du tableau (n) => nbre de personne vivant dans l'hémisphère nord
  },

  MSalaryHS: function (p) {
    //créer une fonction pour calculer la moyenne de salaires des personnes vivant dans l'hémisphère sud
    let n = 0; //définir un objet(n) ou on va ajouter les salaires
    let Hs = this.nb0Hs(p); //définir le tableau des personnes vivant dans l'hémisphère sud
    for (let i of Hs) {
      //boucler (faire défiler les lignes i du tableau (Hs))
      n += parseFloat(i.income.substring(1)); //ajouter les salaires du tableau (Hs)
    }
    let result = n / this.nb0Hs(p).length; //définir result(=>résultat) où on divise la somme de tous les salaires de l'hémisphere sud par le nombre de personnes vivant dans l'hémisphère sud
    return result; //renvoyer le résultat qui est le salaire moyen en hémisphère sud
  },

  // ____________________________________________LEVEL 3
  //
  Wl_BC: function (p) {
    //créer une fonction pour déterminer la personne la plus proche de Bérénice
    let Be = { lat: 0, long: 0 };
    for (let i of p) {
      if (i.first_name.includes("Bérénice") && i.last_name.includes("Cawt")) {
        Be = { lat: i.latitude, long: i.longitude };
      }
    }
    let gap = 9999999999;
    let nearest_us = 0;
    for (let i of p) {
      let allS = { lat: i.latitude, long: i.longitude };

      if (this.Pythagore(allS, Be) !== 0 && this.Pythagore(allS, Be) < gap) {
        gap = this.Pythagore(allS, Be);
        nearest_us = [i.last_name, i.id];
      }
    }
    return nearest_us;
  },

  Wl_RB: function (p) {
    //créer une fonction pour déterminer la personne la plus proche de Ruì
    let Rb = { lat: 0, long: 0 };
    for (let i of p) {
      if (i.first_name.includes("Ruì") && i.last_name.includes("Brach")) {
        Rb = { lat: i.latitude, long: i.longitude };
      }
    }
    let gap = 9999999999;
    let nearest_us = 0;
    for (let i of p) {
      let allS = { lat: i.latitude, long: i.longitude };

      if (this.Pythagore(allS, Rb) !== 0 && this.Pythagore(allS, Rb) < gap) {
        gap = this.Pythagore(allS, Rb);
        nearest_us = [i.last_name, i.id];
      }
    }
    return nearest_us;
  },

  TenPnear_JB: function (p) {
    //créer une fonction pour déterminer les 10 personnes la plus proche de Josée
    let a = { lat: 0, long: 0 };
    for (let i of p) {
      if (i.first_name.includes("Josée") && i.last_name.includes("Boshard")) {
        a = { lat: i.latitude, long: i.longitude };

        let users_gap = [];
        for (let i of p) {
          let b = { long: i.longitude, lat: i.latitude };
          if (this.Pythagore(a, b) !== 0) {
            users_gap.push(this.Pythagore(a, b));
          }
        }
        users_gap.sort((a, b) => a - b);
        users_gap.splice(10, p.length - 10);
        let nearTen = [];

        for (let i of users_gap) {
          for (let x of p) {
            let b = { long: x.longitude, lat: x.latitude };
            if (this.Pythagore(a, b) !== 0 && this.Pythagore(a, b) == i) {
              nearTen.push(x.last_name, x.id);
            }
          }
        }
        return nearTen;
      }
    }
  },
  Gwteam: function (p) {
    //créer une fonction pour calculer les personnes travaillant chez google (23)
    let n = []; //créer un tableau (n)
    for (i of p) {
      //boucler (faire défiler les lignes i du tableau (p))
      if (i.email.includes("google")) {
        //si la ligne email comprend le mot "google"
        n.push(i.last_name, i.id); //ajouter la ligne avec les attributs last_name et id au tableau (n)
      }
    }
    return n; //renvoyer le tableau (n)=> nbre de personnes travaillant chez google
  },

  Older: function (p) {
    //créer une fonction pour déterminer la personne la plus agée
    let dateBolder = new Date();
    let theolder;
    for (let i of p) {
      if (this.dateBirth(i) < dateBolder) {
        dateBolder = this.dateBirth(i);
        theolder = i;
      }
    }

    return [theolder.last_name, theolder.first_name];
  },

  Younger: function (p) {
    //créer une fonction pour déterminer la personne la plus jeune
    let dateByounger = new Date(1900, 01, 01);
    let theyounger;
    for (let i of p) {
      if (this.dateBirth(i) > dateByounger) {
        dateByounger = this.dateBirth(i);
        theyounger = i;
      }
    }
    return [theyounger.last_name, theyounger.first_name];
  },

  // MageM:

  // _______________________________________________LEVEL 4

  // GMovPop:

  // ________________________________________________MATCH
  match: function (p) {
    return "not implemented".red;
  },

  // *******************************************AUTRES FONCTIONS**************************

  nb0Hs: function (p) {
    //créer une fonction pour calculer le nbre de personne vivant dans l'hémisphère sud'
    let n = []; //créer un tableau (n)
    for (i of p) {
      //boucler (faire défiler les lignes i du tableau (p))
      if (i.latitude < 0) {
        //si la ligne i (latitude) est inf à 0(=>équateur)
        n.push(i); //rajouter la ligne i dans le tableau (n)
      }
    }
    return n; //renvoyer le tableau (n) contenant les personne vivant dans l'hémisphère sud
  },
  Salary: function (p) {
    //créer une fonction pour enlever le dollar du salaire
    return parseFloat(p.income.substring(1)); //renvoyer le salaire sans le dollar
  },

  Pythagore: function (a, b) {
    //créer une fonction pour calculer la distance entre 2 points avec Pythagore: (C**2)=(A**2)+(B**2)
    return (
      Math.sqrt(Math.pow(b.long - a.long, 2)) +
      Math.sqrt(Math.pow(b.lat - a.lat, 2))
    );
  },

  dateBirth: function (p) {
    return new Date(p.date_of_birth);
  },
};
